const baseInicial = [
  ["Facundo", "Dominguez", "36997047", "1", 3500, 0],
  ["Roberto", "Anllelini", "14234867", "1", 200, 30],
  ["Javier", "Heiz", "23867345", "1", 4000, 10],
  ["Abril", "Perry", "12239563", "2", 6400, 10],
];

const COSTO_RESIDENCIAL = 2.25;
const COSTO_COMERCIAL = 4.5;

const BENEFICIO_RESIDENCIAL_2000 = 0.9;
const BENEFICIO_RESIDENCIAL_5000 = 0.85;
const BENEFICIO_COMERCIAL_5000 = 0.9;
const BENEFICIO_COMERCIAL_7000 = 0.85;

const baseDeDatos = [];
let resultadosOrdenados = [];
let resultadosFiltrados = [];
let terminoDeBusqueda = "";
let clienteSeleccionado;
let listaAMostrar = 1;

// Manipulo DOM  para mostrar cliente 
const seleccionarCliente = (cliente) => {
  document.getElementById("form-seleccionado-nombre").value = cliente[0];
  document.getElementById("form-seleccionado-apellido").value = cliente[1];
  document.getElementById("form-seleccionado-dni").value = cliente[2];
  if (cliente[3] == 1) {
    document.getElementById("form-seleccionado-tipo").value = "Residencial";
  } else {
    document.getElementById("form-seleccionado-tipo").value = "Comercial";
  }
  document.getElementById("form-seleccionado-consumo").value = cliente[4];
  document.getElementById("form-seleccionado-deuda").value = cliente[5];
  document.getElementById("form-seleccionado-monto").value = cliente[6];
};

// Manipulo DOM 
const actualizarLista = () => {
  const lista = document.getElementById("form-seleccionar-listado");
  const opciones = lista.children;
  while (opciones.length) {
    lista.removeChild(opciones[0]);
  }
  
  resultadosFiltrados.forEach((cliente, index) => {
    const li = document.createElement("li");
    const button = document.createElement("button");
    button.innerHTML = "Seleccionar";
    button.className = "btn--selec"
    button.addEventListener("click", () => seleccionarCliente(cliente));
    li.innerHTML = " NOMBRE: " + cliente[0] + " " + cliente[1] + " - DNI: " + cliente[2] + " ";
    li.appendChild(button);
    lista.appendChild(li);
  });
};

const filtrar = () => {
  let lista = [];
  if (listaAMostrar == 1) {
    lista = baseDeDatos;
  }
  if (listaAMostrar == 2) {
    lista = resultadosOrdenados;
  }
  resultadosFiltrados = lista.filter((el) => {
    if (el[2] == terminoDeBusqueda) {
      return true;
    }
    // Paso a minuscula para que siempre coincida el resultado
    const nombre = String(el[0] + " " + el[1]).toLowerCase();
    return nombre.indexOf(terminoDeBusqueda.toLowerCase()) !== -1;
  });
  actualizarLista();
};

// Separando referencia del objecto original 
const ordenar = () => {
  const clon = JSON.parse(JSON.stringify(baseDeDatos));
  resultadosOrdenados = clon.sort((a, b) => {
    // Paso a minuscula para que al momento de comparar una letra Mayuscula no afecte el ordenamiento
    const nombreA = String(a[0] + " " + a[1]).toLowerCase();
    const nombreB = String(b[0] + " " + b[1]).toLowerCase();

   // Ordenando asendentemente  
    if (nombreA < nombreB) {
      return -1;
    }
    if (nombreA > nombreB) {
      return 1;
    }
    return 0;
  });
};

const calcularMonto = (tipo, consumo = 0, deuda = 0) => {
  let monto = 0;
  if (tipo == 1) {
    monto = consumo * COSTO_RESIDENCIAL;
    // solo da beneficios si no tiene deuda 
    if (deuda == 0) {
      if (consumo >= 5000) {
        monto = monto * BENEFICIO_RESIDENCIAL_5000;
      } else if (consumo >= 2000) {
        monto = monto * BENEFICIO_RESIDENCIAL_2000;
      }
    }
  }
  if (tipo == 2) {
    monto = consumo * COSTO_COMERCIAL;
    // solo da beneficios si no tiene deuda 
    if (deuda == 0) {
      if (consumo >= 7000) {
        monto = monto * BENEFICIO_COMERCIAL_7000;
      } else if (consumo >= 5000) {
        monto = monto * BENEFICIO_COMERCIAL_5000;
      }
    }
  }
  monto = monto + deuda;
  return monto;
};

// Handlers 
const agregarCliente = (ev) => {
  ev.preventDefault();
  ev.stopPropagation();

  const nombre = document.getElementById("form-agregar-nombre").value;
  const apellido = document.getElementById("form-agregar-apellido").value;
  const dni = document.getElementById("form-agregar-dni").value;
  const tipo = document.getElementById("form-agregar-tipo").value;
  const consumo = document.getElementById("form-agregar-consumo").value;
  const deuda = document.getElementById("form-agregar-deuda").value;

  baseDeDatos.push([
    nombre,
    apellido,
    dni,
    tipo,
    consumo,
    deuda,
    calcularMonto(tipo, Number(consumo) || 0, Number(deuda) || 0),
  ]);

  // llamo ordenar y filtrar para actualizar DOM sin necesidad de buscar de nuevo
  ordenar();
  filtrar();
};

const buscarCliente = (ev) => {
  ev.preventDefault();
  ev.stopPropagation();
  const termino = document.getElementById("form-buscar-termino").value;
  terminoDeBusqueda = termino;
  filtrar();
};

const llenarBaseDeDatos = () => {
  baseInicial.forEach((cliente) => {
    const monto = calcularMonto(cliente[3], cliente[4], cliente[5]);
    cliente.push(monto);
    baseDeDatos.push(cliente);
  });
  ordenar();
  filtrar();
};
// cambia el tipo de lista a mostrar 
const cambiarListaAMostrar = (lista) => {
  listaAMostrar = lista;
  filtrar();
  actualizarLista();
};

const mostrarPorCreacion = (ev) => {
  ev.preventDefault();
  ev.stopPropagation();
  cambiarListaAMostrar(1);
};

const mostrarAlfabeticamente = (ev) => {
  ev.preventDefault();
  ev.stopPropagation();
  cambiarListaAMostrar(2);
};
// listeners 
document
  .getElementById("form-agregar")
  .addEventListener("submit", agregarCliente);
document
  .getElementById("form-buscar")
  .addEventListener("submit", buscarCliente);
document
  .getElementById("mostrar-creacion")
  .addEventListener("click", mostrarPorCreacion);
document
  .getElementById("mostrar-alfabeticamente")
  .addEventListener("click", mostrarAlfabeticamente);

window.addEventListener("load", (ev) => {
  llenarBaseDeDatos();
});
